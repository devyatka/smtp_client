import socket
import ssl
import base64
import argparse
from getpass import getpass


def parse_args():
    parser = argparse.ArgumentParser(description='SMTP client for sending emails')
    parser.add_argument('-p', '--pseudo', help='if you want to send email on behalf of somebody else...'
                                               "\nbut just on your own address (maybe later I'll find the way)")
    return parser.parse_args()


def parse_config():
    with open('config.txt', 'r', encoding='utf-8') as file:
        recievers = file.readline().strip('\n').split(' ')
        subject = file.readline().strip('\n')
        attachments = file.readline().strip('\n')
    if attachments == 'None':
        attachments = None
    else:
        attachments = attachments.split(' ')
    return recievers, subject, attachments


def parse_message():
    with open('message.txt', 'r', encoding='utf-8') as file:
        text = file.read()
    return text


def main():
    parser = parse_args()
    smtp_host = ('smtp.yandex.ru', 465)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock = ssl.wrap_socket(sock)
        sock.connect(smtp_host)
        print(sock.recv(1024))
        print(f'connected successfully to {smtp_host[0]}')

        login = base64.b64encode(bytes(input('enter your login for yandex mail service: '), encoding='utf-8'))
        password = base64.b64encode(bytes(getpass('enter password: '), encoding='utf-8'))
        sender = 'anytestac@yandex.ru'
        recievers, subject, attachments = parse_config()
        text = parse_message()

        print(send_recv(bytes(f'EHLO {sender}', encoding='utf-8'), sock))
        print(send_recv(b'AUTH LOGIN', sock))
        print(send_recv(login, sock))
        print(send_recv(password, sock))

        for reciever in recievers:
            print(send_recv(bytes(f'MAIL FROM: {sender}', encoding='utf-8'), sock))
            print(send_recv(bytes(f'RCPT TO: {reciever}', encoding='utf-8'), sock))
            print(send_recv(b'DATA', sock))

            message = create_msg(parser.pseudo if parser.pseudo is not None
                                 else sender, reciever, subject, text, attachments)

            print(send_recv(message, sock))


def send_recv(data, sock):
    data += b'\n'
    sock.send(data)
    return sock.recv(1024).decode(encoding='utf-8')


def create_msg(sender, reciever, subject, text_msg, attachments):
    if text_msg[0] == '.':
        text2 = '..' + text_msg[1:]
    else:
        text2 = ''
    text = text2.replace('\n.', '\n..')
    if attachments is None:
        return bytes(f'From: {sender}'
                     f'\nTo: {reciever}'
                     f'\nSubject: {subject}'
                     f'\nMIME-Version: 1.0'
                     f'\nContent-Type: text/plain'
                     f'\n\n{text}'
                     f'\n.', encoding='utf-8')

    formatted_attachments = []
    boundary = 'myownunictextwhichwontbeinmessageanyway742387238978347'

    for doc in attachments:
        encoded_data = base64.b64encode(open(doc, 'rb').read())
        content_type = get_content_type(doc.split('.')[1])
        formatted_attachments.append(f'Content-Disposition: attachment; filename="{doc}"\n'
                                     'Content-Transfer-Encoding: base64\n'
                                     f'Content-Type: {content_type}; name="{doc}"\n\n'
                                     + encoded_data.decode() + f'\n--{boundary}\n')
    formatted_attachments = ''.join(formatted_attachments)
    message = (f'From: {sender}\n'
               f'To: {reciever}\n'
               f'Subject: {subject}\n'
               'MIME-Version: 1.0\n'
               f'Content-Type: multipart/mixed; boundary="{boundary}"\n\n'
               f'--{boundary}\n'
               'Content-Type: text/plain; charset=utf-8\n'
               'Content-Transfer-Encoding: 8bit\n\n'
               f'{text}\n'
               f'--{boundary}\n'
               f'{formatted_attachments}--\n.')

    return message.encode()


def get_content_type(doc_type):
    types = {'image': {'png': 'png', 'gif': 'gif', 'ico': 'x-icon', 'jpeg': 'jpeg', 'jpg': 'jpeg', 'svg': 'svg+xml',
                       'tiff': 'tiff', 'tif': 'tiff', 'webp': 'webp'},
             'video': {'avi': 'x-msvideo', 'mpeg': 'mpeg', 'ogv': 'ogg', 'webm': 'webm'},
             'application': {'zip': 'zip', 'xml': 'xml', 'bin': 'octet-stream', 'bz': 'x-bzip', 'doc': 'msword',
                             'epub': 'epub+zip', 'js': 'javascript', 'json': 'json', 'pdf': 'pdf',
                             'ppt': 'vnd.ms-powerpoint', 'rar': 'x-rar-compressed', 'sh': 'x-sh', 'tar': 'x-tar'},
             'audio': {'wav': 'x-wav', 'oga': 'ogg'},
             'text': {'css': 'css', 'csv': 'csv', 'html': 'html', 'htm': 'html', 'txt': 'plain'}}
    c_type = ''
    c_subtype = ''
    for content_type, subtypes in types.items():
        for subtype in subtypes:
            if doc_type == subtype:
                c_subtype = subtypes[subtype]
                c_type = content_type
                break
    return f'{c_type}/{c_subtype}'


if __name__ == '__main__':
    main()
